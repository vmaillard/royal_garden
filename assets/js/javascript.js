document.addEventListener("DOMContentLoaded", function() {
	init();
}, false);

function init() {
    
    initTagsClick();

	// registerListener('scroll', highlight_scroll);
    // registerListener('resize', highlight_scroll);

    var col_1 = document.getElementById("colonne_1");
    var col_2 = document.getElementById("colonne_2");
    var col_3 = document.getElementById("colonne_3");
    
    var colonnes = document.querySelectorAll(".colonne");
    colonnes.forEach(function(item) {
        // $(item).scrollTop(20);
        cloneColElements(item);
        var primitive_container = item.getElementsByClassName("primitive_container")[0];
        $(item).animate({
            scrollTop : primitive_container.offsetHeight + 10
        }, 0, 'easeInOutExpo');
    });

    registerListener(col_1, 'scroll', highlight_scroll);
    registerListener(col_1, 'resize', highlight_scroll);

    registerListener(col_2, 'scroll', highlight_scroll);
    registerListener(col_2, 'resize', highlight_scroll);

    registerListener(col_3, 'scroll', highlight_scroll);
    registerListener(col_3, 'resize', highlight_scroll);

}

function initTagsClick() {
    var tags = document.querySelectorAll(".tag");

    tags.forEach(function(item) {
        item.addEventListener('click', scrollToArticle, false);
    });
}


function scrollToArticle() {

    var this_tag = this.getAttribute("data-tag");
    var this_article = this.parentElement.parentElement;
    var this_id = this_article.getAttribute("data-id");
    
    var this_colonne = this.closest(".colonne");
    var this_colonne_number = this.closest(".colonne").getAttribute("data-colonne");    
    
    toggleActive(this_colonne, this);
    toggleCurrent(this_colonne, this_article);

    var this_colonne_jquery = $(this).closest(".colonne");
    var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

    $(this).closest(".colonne").animate({
        scrollTop : $(this).closest("article").offset().top - this_colonne_jquery.offset().top + this_colonne_jquery.scrollTop() - (y/2) + 100
        // scrollTop: tag_to_go.offset().top
    }, 1000, 'easeInOutExpo');

    var col_array = ['1', '2', '3'];
    removeA(col_array, this_colonne_number);
    // console.log(col_array);

    scrollTo(col_array, this_tag, this_id);
}


function scrollTo(array, tag, id) {
    var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

    for (var i = 0; i < array.length; i++) {
        
        var col_neighbour = document.getElementById("colonne_"+array[i]);
        
        var array_tags = [].slice.call(col_neighbour.getElementsByClassName(tag));
        
        // on retire les el qui ont le même id
        for (var j = 0; j < array_tags.length; j++) {
            if (array_tags[j].getAttribute("data-id") == id) {
                array_tags.splice(j, 1);
                j--;
            }
        }

        // pb du clic sur un él qui n'a pas de voisins
        // il faut retirer les .current

        $("#colonne_"+array[i]).finish();
        if (array_tags.length) {
            var random_index = getRandomInt(0,array_tags.length - 1);
            el_neighbour = array_tags[random_index];
            el_neighbour_tag = el_neighbour.querySelector('[data-tag="'+tag+'"]');
            
            toggleActive(col_neighbour, el_neighbour_tag);
            toggleCurrent(col_neighbour, el_neighbour);
    
            $("#colonne_"+array[i]).animate({
                scrollTop : el_neighbour.getBoundingClientRect().top - $("#colonne_"+array[i]).offset().top + $("#colonne_"+array[i]).scrollTop() - (y/2) + 100
            }, 1000, 'easeInOutExpo');
        } else {
            // pas de voisins
            toggleCurrent(col_neighbour);
            toggleActive(col_neighbour);
        }
        
    }
}

function toggleActive(col, el) {
    var old_active = col.querySelector(".active");
    if (old_active) old_active.classList.remove("active");
    if (el) el.classList.add("active");
}

function toggleCurrent(col, el) {
    var old_current = col.getElementsByClassName("current")[0];
    if (old_current) old_current.classList.remove("current");
    if (el) el.classList.add("current");
}




function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}





function registerListener(el, event, func) {
    if (el) {
    if (el.addEventListener) {
        el.addEventListener(event, throttle(func, 300));
    } else {
        el.attachEvent('on' + event, func)
    }
    }
}

function highlight_scroll() {
    var this_scroll_top  = this.pageYOffset || this.scrollTop;
    var this_height = this.clientHeight;
    var this_scroll_height = this.scrollHeight;

    
    if (this_scroll_top < 10) {
        cloneColElements(this, true);
    }
    if (this_scroll_top + this_height >= this_scroll_height) {
        cloneColElements(this);
    }

}

function cloneColElements(col, prepend) {
    var this_primitive_container = col.getElementsByClassName("primitive_container")[0];
    var clone_container = this_primitive_container.cloneNode(true);
    var tableau = [];

    for (var i = 0; i < clone_container.childNodes.length; i++) {
        if (clone_container.childNodes[i].nodeName == "ARTICLE") {
            clone_container.childNodes[i].classList.remove("current");
            tableau.push(clone_container.childNodes[i]);
        }
    }
    var new_tableau = shuffleArray(tableau);
    if (prepend) {
        for (var i = 0; i < new_tableau.length; i++) {
            col.prepend(new_tableau[i]);
        }

        if( $(col).is(':animated') ) {
            // ne rien faire
            console.log("ANIM");
            setTimeout(function(){
                $(col).animate({
                    scrollTop : (2*this_primitive_container.offsetHeight) - 20
                }, 1000, 'easeInOutExpo');
            }, 1000);

        } else {
            $(col).animate({
                scrollTop : this_primitive_container.offsetHeight + 10
            }, 0, 'easeInOutExpo');
        }


        
    } else {
        for (var i = 0; i < new_tableau.length; i++) {
            col.appendChild(new_tableau[i]);
        }
    }
    initTagsClick();
}


var ecoute_scroll = function(e) {
    
    var first_main = document.getElementById("first_main");
    if (isEnd()) {
        //console.log("end");
        var clone_main = first_main.cloneNode(true);
        var tableau = [];

        for (var i = 0; i < clone_main.childNodes.length; i++) {
            if (clone_main.childNodes[i].nodeName == "A") {
                tableau.push(clone_main.childNodes[i]);
            }
        }
        var new_tableau = shuffleArray(tableau);
        var main = document.querySelector("main");
        for (var i = 0; i < new_tableau.length; i++) {
            main.appendChild(new_tableau[i]);
        }
    }

};

function shuffleArray(array) {
    for (var i = array.length - 2; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}


// fonction pour alléger l'event scroll
var throttle = function(func, wait) {

    var context, args, timeout, throttling, more, result;
    return function() {
      context = this; args = arguments;
      var later = function() {
        timeout = null;
        if (more) func.apply(context, args);
      };
      if (!timeout) timeout = setTimeout(later, wait);
      if (throttling) {
        more = true;
      } else {
        result = func.apply(context, args);
      }
      throttling = true;
      return result;
    };
};



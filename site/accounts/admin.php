<?php if(!defined('KIRBY')) exit ?>

username: admin
password: >
  $2y$10$5JL4Rq.OF/WIbeMcXGk3YuWeiBeaLqtNGDeKUtVyLId2aCpEgT9nG
email: vincent.maillard.contact@gmail.com
language: fr
role: admin
history:
  - home
  - bibliographie
  - oeuvres/oeuvre-3
  - oeuvres/oeuvre-1
  - oeuvres/oeuvre-8

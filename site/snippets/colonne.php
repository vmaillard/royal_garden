<?php



// $notices_random = page('notices')->children()->shuffle();

$annotations = page('notices')->children()->filterBy('template', 'in', ['annotation'])->shuffle();
$notices = page('notices')->children()->children()->shuffle();

$notices_fusion = new Pages(array($annotations, $notices));
$notices_random = $notices_fusion->shuffle();

?>
<?php foreach($notices_random as $notice): ?>

<?php if($notice->template() == 'entrée'): ?>

  <?php include 'notice.php'; ?>

<?php else: ?>

  <?php include 'annotation.php'; ?>

<?php endif; ?>

<?php endforeach ?>
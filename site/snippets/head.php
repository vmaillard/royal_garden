<!doctype html>
<html lang="fr">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <title><?= $site->title()->html() ?> | <?= $page->title()->html() ?></title>
  <meta name="description" content="<?= $site->description()->html() ?>">

  <?= css('assets/css/main.css') ?>
  
  <?= js('assets/js/javascript.js', ['defer' => true]) ?>

  <?= js(array(
    'assets/js/jquery-2.2.4.min.js',
    'assets/js/jquery-ui.min.js',
    'assets/js/javascript.js',
    'assets/js/main_scroll.js',
    'assets/js/element-closest.js'
  ),
  ['defer' => true]
  )
  ?>


</head>
<body>

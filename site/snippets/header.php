<header>

  <nav>
    <select onchange="window.location.href = this.value">
<?php foreach($pages->visible() as $page): ?>
<?php if($page != $pages->find('notices')): ?>
    <option value="<?= $page->url() ?>"<?php e($page->isOpen(), ' selected="selected"') ?>><?= $page->title()->html() ?></option>
<?php endif; ?>
<?php endforeach ?>
    </select>
  </nav>

</header>
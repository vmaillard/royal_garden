<?php

$array = explode(", ", str_replace('tag: ', '', $notice->tags_collection()));
$array_corrected = [];
foreach($array as $tag) {
    $tag = str_replace(' ', '-', strtolower($tag));
    
    array_push($array_corrected, $tag);
}

$array_corrected_join = join(' ', $array_corrected);
/*
$array = $notice->tags_collection()->split();
$array_corrected = [];
foreach($array as $tag) {
  $tag = str_replace(' ', '-', strtolower($tag));
  array_push($array_corrected, $tag);
}
$array_corrected_join = join(' ', $array_corrected);
*/
?>

    <article class="text_notice <?= $array_corrected_join ?>" data-id="<?= $notice->id() ?>">

        <?= $notice->text()->kirbytext() ?> 

        <div class="tags_container">
    <?php foreach($array as $key=>$tag): ?>
        <span class="tag" data-tag="<?= $array_corrected[$key] ?>">#<?= $tag ?></span>
    <?php endforeach ?>
        </div>
    </article>
<?php 
function shuffle_assoc($array) {
  $keys = array_keys($array);

  shuffle($keys);

  foreach($keys as $key) {
      $new[$key] = $array[$key];
  }

  $array = $new;

  return true;
}

snippet('header') ?>

<main>
  <div class="colonne" id="colonne_1" data-colonne="1">
    <div class="primitive_container">
<?php snippet('colonne') ?>
    </div>
  </div>

  <div class="colonne" id="colonne_2" data-colonne="2">
    <div class="primitive_container">
<?php snippet('colonne') ?>
    </div>
  </div>

  <div class="colonne" id="colonne_3" data-colonne="3">
    <div class="primitive_container">
<?php snippet('colonne') ?>
    </div>
  </div>

</main>

<?php snippet('footer') ?>
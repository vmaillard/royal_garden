<?php snippet('head') ?>

<? snippet('header') ?>

<main class="container_glossaire">

  <?php foreach($page->children() as $oeuvre): ?>
    <article class="oeuvre">

<?php foreach($oeuvre->slideshow()->yaml() as $image): ?>   
  <?php if($image = $oeuvre->image($image)): ?>
      <div>
        <figure>
          <?= $image->html() ?>
        </figure>
      </div>
  <?php endif ?>
<?php endforeach; ?>

      <div>
        <h2><?=$oeuvre->title()?></h2>
<?php if($oeuvre->artist()->isNotEmpty()): ?>
        <p><?=$oeuvre->artist()?></p>
<?php endif ?>
<?php if($oeuvre->datation()): ?>
        <p><?=$oeuvre->datation()?></p>
<?php endif ?>
<?php if($oeuvre->notice()->isNotEmpty()): ?>
        <?=$oeuvre->notice()->kirbytext()?>
<?php endif ?>
      </div>
      
    </article>
  <?php endforeach ?>

</main>

<?php snippet('footer') ?>